from urllib import parse
from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_cors import CORS

from resources.user import (
    UserRegister,
    User,
    UserLogin,
    UserLogout,
    TokenRefresh,
)

from resources.quiz import Quiz, QuizList
from resources.option import Option, OptionList
from resources.poll import Poll, PollList
from resources.vote import Vote, VoteList, VoteResult
from blacklist import BLACKLIST

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'

params = parse.quote_plus('DRIVER={SQL Server};SERVER=NAHID;DATABASE=PollSystemTest;Trusted_Connection=yes;')
app.config['SQLALCHEMY_DATABASE_URI'] = "mssql+pyodbc:///?odbc_connect=%s" % params

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
app.secret_key = 'slim-shady'
api = Api(app)

@app.before_first_request
def create_tables():
    from db import db
    db.create_all()

jwt = JWTManager(app)

@jwt.user_claims_loader
def add_claims_to_jwt(identity):
    from models.user import UserModel
    user = UserModel.find_by_id(identity)
    if not user:
        return { 'msg': 'Invalid user' }
    return user.role

@jwt.token_in_blacklist_loader
def chcek_if_token_in_blacklist(decrypted_token):
    return decrypted_token['jti'] in BLACKLIST

@jwt.expired_token_loader
def expired_token_callback():
    return jsonify({
        'description': 'The token has expired',
        'error': 'token expired'
    }), 401

@jwt.invalid_token_loader
def invalid_token_callback(error):
    return jsonify({
        'description': 'Signature verification failed',
        'error': 'invalid token'
    }), 401

@jwt.unauthorized_loader
def missing_token_callback(error):
    return jsonify({
        'description': 'Request does not contains an access token',
        'error': 'authorization_required'
    }), 401

@jwt.needs_fresh_token_loader
def token_not_fresh_callback():
    return jsonify({
        'description': 'This token is not fresh',
        'error': 'fresh_token_required'
    }), 401

@jwt.revoked_token_loader
def revoked_token_callback():
    return jsonify({
        'description': 'This token has been revoked',
        'error': 'token_revoked'
    }), 401

api.add_resource(Poll, '/poll')
api.add_resource(PollList, '/polls')

api.add_resource(Quiz, '/quiz')
api.add_resource(QuizList, '/quizes')

api.add_resource(Option, '/option')
api.add_resource(OptionList, '/options')

api.add_resource(Vote, '/vote')
api.add_resource(VoteList, '/votes')
api.add_resource(VoteResult, '/vote_result')

api.add_resource(UserRegister, '/register')
api.add_resource(User, '/user/<int:user_id>')
api.add_resource(UserLogin, '/login')
api.add_resource(TokenRefresh, '/refresh')
api.add_resource(UserLogout, '/logout')

if __name__ == '__main__':
    from db import db
    db.init_app(app)
    app.run(port=5000, debug=True, use_reloader=False)
