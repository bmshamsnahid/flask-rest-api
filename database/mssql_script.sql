
CREATE TABLE [dbo].[Poll](
	[PollId] [int] IDENTITY(1,1) NOT NULL,
	[PollTitle] [nvarchar](100) NULL,
 CONSTRAINT [PK_Poll] PRIMARY KEY CLUSTERED
(
	[PollId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Quiz](
	[QuizId] [int] IDENTITY(1,1) NOT NULL,
	[QuizTitle] [nvarchar](100) NULL,
	[QuizType] [varchar](50) NULL,
	[PollId] [int] NULL,
 CONSTRAINT [PK_Quiz] PRIMARY KEY CLUSTERED
(
	[QuizId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[QuizOption](
	[OptionId] [int] IDENTITY(1,1) NOT NULL,
	[OptionTitle] [nvarchar](10) NULL,
	[QuizId] [int] NULL,
 CONSTRAINT [PK_QuizOption] PRIMARY KEY CLUSTERED
(
	[OptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[UserType] [varchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE TABLE [dbo].[Vote](
	[VoteId] [int] IDENTITY(1,1) NOT NULL,
	[VoteTime] [datetime] NULL,
	[UserId] [int] NULL,
	[QuizId] [int] NULL,
	[OptionId] [int] NULL,
 CONSTRAINT [PK_Vote] PRIMARY KEY CLUSTERED
(
	[VoteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Vote] ADD  CONSTRAINT [DF_Vote_VoteTime]  DEFAULT (getdate()) FOR [VoteTime]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Poll] FOREIGN KEY([PollId])
REFERENCES [dbo].[Poll] ([PollId])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Poll]
GO
ALTER TABLE [dbo].[QuizOption]  WITH CHECK ADD  CONSTRAINT [FK_QuizOption_Quiz] FOREIGN KEY([QuizId])
REFERENCES [dbo].[Quiz] ([QuizId])
GO
ALTER TABLE [dbo].[QuizOption] CHECK CONSTRAINT [FK_QuizOption_Quiz]
GO
ALTER TABLE [dbo].[Vote]  WITH CHECK ADD  CONSTRAINT [FK_Vote_Quiz] FOREIGN KEY([QuizId])
REFERENCES [dbo].[Quiz] ([QuizId])
GO
ALTER TABLE [dbo].[Vote] CHECK CONSTRAINT [FK_Vote_Quiz]
GO
ALTER TABLE [dbo].[Vote]  WITH CHECK ADD  CONSTRAINT [FK_Vote_QuizOption] FOREIGN KEY([OptionId])
REFERENCES [dbo].[QuizOption] ([OptionId])
GO
ALTER TABLE [dbo].[Vote] CHECK CONSTRAINT [FK_Vote_QuizOption]
GO
ALTER TABLE [dbo].[Vote]  WITH CHECK ADD  CONSTRAINT [FK_Vote_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Vote] CHECK CONSTRAINT [FK_Vote_User]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'radio, dropdown, checkbox' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Quiz', @level2type=N'COLUMN',@level2name=N'QuizType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User, Admin, SuperAdmin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'UserType'
GO