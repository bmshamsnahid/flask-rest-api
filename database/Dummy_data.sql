USE [PollSystem]
GO
SET IDENTITY_INSERT [dbo].[Poll] ON 

GO
INSERT [dbo].[Poll] ([PollId], [PollTitle]) VALUES (1, N'Health Survey')
GO
SET IDENTITY_INSERT [dbo].[Poll] OFF
GO
SET IDENTITY_INSERT [dbo].[Quiz] ON 

GO
INSERT [dbo].[Quiz] ([QuizId], [QuizTitle], [QuizType], [PollId]) VALUES (1, N'Gender', N'dropdown', 1)
GO
INSERT [dbo].[Quiz] ([QuizId], [QuizTitle], [QuizType], [PollId]) VALUES (2, N'Blood Group', N'radio', 1)
GO
INSERT [dbo].[Quiz] ([QuizId], [QuizTitle], [QuizType], [PollId]) VALUES (3, N'Favorite Foods', N'checkbox', 1)
GO
SET IDENTITY_INSERT [dbo].[Quiz] OFF
GO
SET IDENTITY_INSERT [dbo].[QuizOption] ON 

GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (1, N'Male', 1)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (2, N'Female', 1)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (3, N'A+', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (4, N'A-', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (5, N'B+', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (6, N'B-', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (7, N'AB+', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (8, N'AB-', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (9, N'O+', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (10, N'O-', 2)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (11, N'Meat', 3)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (12, N'Milk', 3)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (13, N'Honey', 3)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (14, N'Sugar', 3)
GO
INSERT [dbo].[QuizOption] ([OptionId], [OptionTitle], [QuizId]) VALUES (15, N'Vegetable', 3)
GO
SET IDENTITY_INSERT [dbo].[QuizOption] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [UserType]) VALUES (1, N'Harun', NULL, N'SuperAdmin')
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [UserType]) VALUES (2, N'Nahid', NULL, N'Admin')
GO
INSERT [dbo].[User] ([UserId], [UserName], [Email], [UserType]) VALUES (3, N'Rayhan', NULL, N'User')
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[Vote] ON 

GO
INSERT [dbo].[Vote] ([VoteId], [VoteTime], [UserId], [QuizId], [OptionId]) VALUES (1, CAST(0x0000A9D700D08934 AS DateTime), 1, 1, 1)
GO
INSERT [dbo].[Vote] ([VoteId], [VoteTime], [UserId], [QuizId], [OptionId]) VALUES (2, CAST(0x0000A9D700D0AE5B AS DateTime), 1, 2, 7)
GO
INSERT [dbo].[Vote] ([VoteId], [VoteTime], [UserId], [QuizId], [OptionId]) VALUES (3, CAST(0x0000A9D700D0D33A AS DateTime), 1, 3, 12)
GO
INSERT [dbo].[Vote] ([VoteId], [VoteTime], [UserId], [QuizId], [OptionId]) VALUES (4, CAST(0x0000A9D700D0DACB AS DateTime), 1, 3, 15)
GO
SET IDENTITY_INSERT [dbo].[Vote] OFF
GO
