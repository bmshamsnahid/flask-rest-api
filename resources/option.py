from flask_restful import Resource, reqparse
from flask import request
from flask_jwt_extended import jwt_required

import helper
from models.option import OptionModel

class Option(Resource):

    @jwt_required
    def get(self):
        if not helper.check_authority('user'):
            return { 'msg': 'Admin previllage required' }

        args = request.args
        id = args['id']

        if not id:
            return { 'message': 'Invalid id.' }, 404

        option = OptionModel.find_by_id(args['id'])

        if option:
            return option.json(), 201

        return { 'message': 'Option not found.' }, 404

    @jwt_required
    def post(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required' }

        parser = reqparse.RequestParser()
        parser.add_argument('name',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        parser.add_argument('quiz_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        data = parser.parse_args()

        name = data['name']
        quiz_id = data['quiz_id']

        if OptionModel.find_by_name(name, quiz_id):
            return { 'message': f'An option with name {name} already exists.' }, 400

        option = OptionModel(name, quiz_id)

        try:
            option.save_to_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'An error occoured inserting the option.' }, 500

        return option.json(), 201

    @jwt_required
    def put(self):
        if not helper.check_authority('user'):
            return { 'msg': 'Admin previllage required' }

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id.' }, 404
        option = OptionModel.find_by_id(args['id'])

        if not option:
            return { 'message': 'Invalid id.' }, 404

        parser = reqparse.RequestParser()
        parser.add_argument('name',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        parser.add_argument('quiz_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        data = parser.parse_args()

        option.name = data['name']
        option.quiz_id = data['quiz_id']

        try:
            option.save_to_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'An error occoured updating the option.' }, 500

        return option.json(), 201

    @jwt_required
    def delete(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required' }

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id.' }, 404

        option = OptionModel.find_by_id(args['id'])
        if option:
            option.delete_from_db()

        return { 'message': 'Option deleted'}, 201


class OptionList(Resource):
    @jwt_required
    def get(self):

        if not helper.check_authority('user'):
            return { 'msg': 'Admin previllage required' }

        args = request.args
        quiz_id = args['id']

        if not quiz_id:
            return { 'message': 'Invalid quiz id.' }, 404

        return {'options': [
            option.json() for option in OptionModel.find_by_quiz_id(quiz_id)
        ]}
