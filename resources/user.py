
from flask_restful import Resource, reqparse
from werkzeug.security import (
    generate_password_hash,
    check_password_hash
)
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity,
    jwt_required,
    get_raw_jwt,
)

from models.user import UserModel
from blacklist import BLACKLIST

_user_parser = reqparse.RequestParser()
_user_parser.add_argument('username',
    type=str,
    required=True,
    help='This field can not be blank'
)
_user_parser.add_argument('password',
    type=str,
    required=True,
    help='This field can not be blank'
)
_user_parser.add_argument('email',
    type=str,
    required=False,
    help='This field can not be blank'
)
_user_parser.add_argument('role',
    type=str,
    required=False,
    help='This field can not be blank'
)

class UserRegister(Resource):

    def post(self):
        data = _user_parser.parse_args()

        if not data['username'] or len(data['username']) < 5:
            return { 'message': 'Invalid username' }, 400

        if not data['password'] or len(data['password']) < 5:
            return { 'message': 'Invalid password' }, 400

        if not data['email'] or len(data['email']) < 5:
            return { 'message': 'Invalid email' }, 400

        if not data['role'] or len(data['role']) < 4:
            return { 'message': 'Invalid role.' }, 400

        if data['role'] not in ['user', 'admin', 'super_admin']:
            return { 'message': 'Role must be [user], [admin] or [super_admin]' }, 400

        if UserModel.find_by_username(data['username']):
            return { 'message': 'A user with that username already exists.' }, 400

        if UserModel.find_by_email(data['email']):
            return { 'message': 'A user with that email already exists.' }, 400

        username = data['username']
        password = generate_password_hash(data['password'])
        email = data['email']
        role = data['role']

        user = UserModel(username, password, email, role)
        user.save_to_db()

        return { 'message': 'User created successfully.' }, 201


class User(Resource):
    @classmethod
    def get(cls, user_id):
        user = UserModel.find_by_id(user_id)
        if not user:
            return { 'message': 'User not found' }, 404
        return user.json()

    @classmethod
    def delete(cls, user_id):
        user = UserModel.find_by_id(user_id)
        if not user:
            return { 'message': 'User not found' }, 404
        user.delete_from_db()
        return { 'message': 'user deleted' }


class UserLogin(Resource):

    @classmethod
    def post(cls):
        # get data from parser
        data = _user_parser.parse_args()

        # find user in database
        user = UserModel.find_by_username(data['username'])

        # check password
        # create access token
        # create refresh token
        # return them
        if user and check_password_hash(user.password, data['password']):
            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(user.id)
            return {
                'access_token': access_token,
                'refresh_token': refresh_token,
                'user_id': user.id,
            }, 200
        return { 'message': 'Invalid credentials.' }, 401


class UserLogout(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        BLACKLIST.add(jti)
        return { 'message': 'Successfully logged out.' }, 200



class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return { 'access_token': new_token }, 200
