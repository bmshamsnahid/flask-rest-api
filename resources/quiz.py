from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required
from flask import request

import helper
from models.quiz import QuizModel

class Quiz(Resource):

    @jwt_required
    def get(self):
        if not helper.check_authority('user'):
            return { 'msg': 'User previllage required' }

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id.' }, 404

        quiz = QuizModel.find_by_id(args['id'])

        if quiz:
            return quiz.json()

        return { 'message': 'Quiz not found.' }, 404

    @jwt_required
    def post(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required' }

        parser = reqparse.RequestParser()
        parser.add_argument('name',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('type',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('poll_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        data = parser.parse_args()

        name = data['name']
        type = data['type']
        poll_id = data['poll_id']

        if type not in ['radio', 'drop_down_list', 'check_box']:
            return { 'message': 'Quiz type must be [radio], [drop_down_list] or [check_box]' }, 400

        if QuizModel.find_by_name(name, poll_id):
            return { 'message': f'An quiz with name {name} already exists.' }, 400

        quiz = QuizModel(name, type, poll_id)

        try:
            quiz.save_to_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'An error occoured inserting the item.' }, 500

        return quiz.json(), 201

    @jwt_required
    def put(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required' }, 404

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id.' }, 404
        quiz = QuizModel.find_by_id(args['id'])

        parser = reqparse.RequestParser()
        parser.add_argument('name',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('type',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('poll_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        data = parser.parse_args()

        name = data['name']
        type = data['type']
        poll_id = data['poll_id']

        if type not in ['radio', 'drop_down_list', 'check_box']:
            return { 'message': 'Quiz type must be [radio], [drop_down_list] or [check_box]' }, 400

        quiz.name = name
        quiz.type = type
        quiz.poll_id = poll_id

        try:
            quiz.save_to_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'An error occoured inserting the item.' }, 500

        return quiz.json(), 201

    @jwt_required
    def delete(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required' }, 404

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id.' }, 404

        quiz = QuizModel.find_by_id(args['id'])
        if quiz:
            quiz.delete_from_db()

        return {
            'id': args['id'],
            'message': 'quiz deleted'
        }, 201


class QuizList(Resource):

    @jwt_required
    def get(self):
        if not helper.check_authority('user'):
            return { 'msg': 'User previllage required' }, 404

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id.' }, 404

        quizes = QuizModel.find_by_poll_id(args['id'])

        return {'quizes': [
            quiz.json() for quiz in quizes
        ]}
