from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required
from flask import request

import helper
from models.vote import VoteModel

class Vote(Resource):

    @jwt_required
    def get(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'User previllage required' }

        args = request.args

        if not args['id']:
            return { 'message': 'Invalid id.' }, 404

        vote = VoteModel.find_by_id(args['id'])

        if vote:
            return vote.json()

        return { 'message': 'Vote not found.' }, 404

    @jwt_required
    def post(self):
        if not helper.check_authority('user'):
            return { 'msg': 'User previllage required' }

        parser = reqparse.RequestParser()
        parser.add_argument('user_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('poll_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('quiz_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('option_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        data = parser.parse_args()

        user_id = data['user_id']
        poll_id = data['poll_id']
        quiz_id = data['quiz_id']
        option_id = data['option_id']

        # vote = VoteModel.find_by_user_id_and_quiz_id(user_id, quiz_id)

        vote = VoteModel(user_id, poll_id, quiz_id, option_id)

        # if not vote:
        #     vote = VoteModel(user_id, quiz_id, option_id)
        # else:
        #     vote.option_id = option_id

        try:
            vote.save_to_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'An error occoured inserting the item.' }, 500

        return vote.json(), 201

    @jwt_required
    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )
        parser.add_argument('poll_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        data = parser.parse_args()

        user_id = data['user_id']
        poll_id = data['poll_id']

        votes = VoteModel.find_by_user_id_and_poll_id(user_id, poll_id)

        for vote in votes:
            vote.delete_from_db()

        return {
            'messgae': 'User previous votes removed.'
        }, 201


class VoteList(Resource):

    @jwt_required
    def get(self):
        if not helper.check_authority('super_admin'):
            return { 'msg': 'Super Admin previllage required' }

        return {'votes': [
                vote.json() for vote in VoteModel.query.all()
            ]}


class VoteResult(Resource):

    @jwt_required
    def get(self):
        if not helper.check_authority('user'):
            return { 'msg': 'User previllage required' }

        args = request.args

        quiz_id = args['id']

        if not quiz_id:
            return { 'message': 'Invalid quiz id.' }, 404

        return {
            'votes': [
                vote.json() for vote in VoteModel.find_by_quiz_id(quiz_id)
            ]
        }

    @jwt_required
    def post(self):
        if not helper.check_authority('user'):
            return { 'msg': 'User previllage required' }

        parser = reqparse.RequestParser()

        parser.add_argument('poll_id',
            type=str,
            required=True,
            help='This field can not be left blank'
        )

        data = parser.parse_args()

        poll_id = data['poll_id']

        votes = VoteModel.find_by_poll_id(poll_id)

        return {
            'votes': [
                vote.json() for vote in votes
            ]
        }
