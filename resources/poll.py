from flask_restful import (
    Resource,
    reqparse,
)
from flask_jwt_extended import jwt_required
from flask import request

import helper
from models.poll import PollModel

class Poll(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('title',
        type=str,
        required=True,
        help='This field can not be left blank'
    )

    @jwt_required
    def get(self):
        if not helper.check_authority('user'):
            return { 'msg': 'User previllage required' }, 404

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id' }, 404

        poll = PollModel.find_by_id(args['id'])

        if poll:
            return poll.json()

        return { 'message': 'Poll not found.' }, 404

    @jwt_required
    def post(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required' }, 404

        data = Poll.parser.parse_args()
        title = data['title']

        if PollModel.find_by_title(title):
            return { 'message': f'An poll with title {title} already exists.' }, 404

        poll = PollModel(title)

        try:
            poll.save_to_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'Inrernal server error.' }, 500

        return poll.json()

    @jwt_required
    def put(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required' }, 404

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id' }, 404

        poll = PollModel.find_by_id(args['id'])

        data = Poll.parser.parse_args()
        title = data['title']

        poll.title = title

        try:
            poll.save_to_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'Inrernal server error.' }, 500

        return poll.json(), 200

    @jwt_required
    def delete(self):
        if not helper.check_authority('admin'):
            return { 'msg': 'Admin previllage required.' }, 404

        args = request.args
        if not args['id']:
            return { 'message': 'Invalid id' }, 404

        poll = PollModel.find_by_id(args['id'])

        if not poll:
            return { 'message': 'Poll does not exists.' }, 404

        try:
            poll.delete_from_db()
        except Exception as ex:
            print(ex)
            return { 'message': 'Inrernal server error.' }

        return {
            'message': 'Poll deleted.',
            'id': args['id']
        }, 200


class PollList(Resource):

    @jwt_required
    def get(self):
        if not helper.check_authority('user'):
            return { 'msg': 'User previllage required' }

        return {
            'polls': [
                poll.json() for poll in PollModel.query.all()
            ]
        }
