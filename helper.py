from flask_jwt_extended import get_jwt_claims

def check_authority(role):

    claim_role = get_jwt_claims()

    # print()
    # print('Required: ', role)
    # print('Received: ', claim_role)

    if (claim_role == 'super_admin') and (role == 'super_admin' or role == 'admin' or role == 'user'):
        return True
    elif (claim_role == 'admin') and (role == 'admin' or role == 'user'):
        return True
    elif claim_role == 'user' and role == 'user':
        return True

    return False
