from db import db
import datetime

class VoteModel(db.Model):
    __tablename__ = 'votes'

    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    User = db.relationship('UserModel')

    poll_id = db.Column(db.Integer, db.ForeignKey("polls.id"))
    Poll = db.relationship('PollModel')

    quiz_id = db.Column(db.Integer, db.ForeignKey("quizes.id"))
    Quiz = db.relationship('QuizModel')

    option_id = db.Column(db.Integer, db.ForeignKey('options.id'))
    Option = db.relationship('OptionModel')

    def __init__(self, user_id, poll_id, quiz_id, option_id):
        # self.time = VoteModel.time
        self.user_id = user_id
        self.poll_id = poll_id
        self.quiz_id = quiz_id
        self.option_id = option_id

    def json(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'poll_id': self.poll_id,
            'quiz_id': self.quiz_id,
            'option_id': self.option_id,
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_poll_id(cls, poll_id):
        return cls.query.filter_by(poll_id=poll_id).all()

    @classmethod
    def find_by_quiz_id(cls, quiz_id):
        return cls.query.filter_by(quiz_id=quiz_id).all()

    @classmethod
    def find_by_option_id(cls, option_id):
        return cls.query.filter_by(option_id=option_id).all()

    @classmethod
    def find_by_user_id(cls, user_id):
        return cls.query.filter_by(user_id=user_id).all()

    @classmethod
    def find_by_user_id_and_poll_id(cls, user_id, poll_id):
        return cls.query.filter_by(user_id=user_id, poll_id=poll_id).all()

    @classmethod
    def find_by_user_id_and_quiz_id(cls, user_id, quiz_id):
        return cls.query.filter_by(user_id=user_id, quiz_id=quiz_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
