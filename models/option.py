from db import db

from models.vote import VoteModel

class OptionModel(db.Model):
    __tablename__ = 'options'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    quiz_id = db.Column(db.Integer, db.ForeignKey('quizes.id'))
    Quiz = db.relationship('QuizModel')

    def __init__(self, name, quiz_id):
        self.name = name
        self.quiz_id = quiz_id

    def json(self):
        return {
            'id': self.id,
            'name': self.name,
            'quiz_id': self.quiz_id,
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_name(cls, name, quiz_id):
        return cls.query.filter_by(name=name, quiz_id=quiz_id).first()

    @classmethod
    def find_by_quiz_id(cls, quiz_id):
        return cls.query.filter_by(quiz_id=quiz_id).all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        votes = VoteModel.find_by_option_id(self.id)
        for vote in votes:
            vote.delete_from_db()

        db.session.delete(self)
        db.session.commit()
