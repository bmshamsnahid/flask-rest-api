from db import db

from models.option import OptionModel

class QuizModel(db.Model):
    __tablename__ = 'quizes'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    type = db.Column(db.String(50))

    options = db.relationship('OptionModel',  lazy='dynamic')

    poll_id = db.Column(db.Integer, db.ForeignKey('polls.id', ondelete='CASCADE'))
    Poll = db.relationship('PollModel')

    def __init__(self, name, type, poll_id):
        self.name = name
        self.type = type
        self.poll_id = poll_id

    def json(self):
        return {
            'id': self.id,
            'name': self.name,
            'type': self.type,
            'poll_id': self.poll_id,
            'options': [ option.json() for option in self.options.all() ],
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_name(cls, name, poll_id):
        return cls.query.filter_by(name=name, poll_id=poll_id).first()

    @classmethod
    def find_by_type(cls, type, poll_id):
        return cls.query.filter_by(type=type, poll_id=poll_id).all()

    @classmethod
    def find_by_poll_id(cls, poll_id):
        return cls.query.filter_by(poll_id=poll_id).all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        options = OptionModel.find_by_quiz_id(self.id)
        for option in options:
            option.delete_from_db()

        db.session.delete(self)
        db.session.commit()
