from db import db

from models.quiz import QuizModel
from models.vote import VoteModel

class PollModel(db.Model):
    __tablename__ = 'polls'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80))

    quizes = db.relationship('QuizModel', lazy='dynamic')

    def __init__(self, title):
        self.title = title

    def json(self):
        return {
            'id': self.id,
            'title': self.title,
            'quizes': [ quiz.json() for quiz in self.quizes.all() ]
        }

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_title(cls, title):
        return cls.query.filter_by(title=title).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        quizes = QuizModel.find_by_poll_id(self.id)
        for quiz in quizes:
            quiz.delete_from_db()

        db.session.delete(self)
        db.session.commit()
