from models.poll import PollModel
from tests.unit.unit_base_test import UnitBaseTest

class PollTest(UnitBaseTest):
    def test_create_poll(self):
        poll = PollModel('test_title')
        self.assertEqual(poll.title, 'test_title')

    def test_poll_json(self):
        poll = PollModel('test_title')
        expected = {
            'id': None,
            'title': 'test_title',
            'quizes': []
        }
        self.assertDictEqual(expected, poll.json())
