from models.option import OptionModel
from tests.unit.unit_base_test import UnitBaseTest

class OptionTest(UnitBaseTest):
    def test_create_option(self):
        option = OptionModel('test_name', 1)
        self.assertEqual(option.name, 'test_name')
        self.assertEqual(option.quiz_id, 1)

    def test_option_json(self):
        option = OptionModel('test_name', 1)
        expected = {
            'id': None,
            'name': 'test_name',
            'quiz_id': 1
        }
        self.assertDictEqual(expected, option.json())
