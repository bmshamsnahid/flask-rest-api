from models.vote import VoteModel
from tests.unit.unit_base_test import UnitBaseTest

class VoteTest(UnitBaseTest):
    def test_create_vote(self):
        vote = VoteModel(1, 2, 3, 4)
        self.assertEqual(vote.user_id, 1)
        self.assertEqual(vote.poll_id, 2)
        self.assertEqual(vote.quiz_id, 3)
        self.assertEqual(vote.option_id, 4)


    def test_vote_json(self):
        vote = VoteModel(1, 2, 3, 4)
        expected = {
            'id': None,
            'user_id': 1,
            'poll_id': 2,
            'quiz_id': 3,
            'option_id': 4,

        }
        self.assertEqual(expected, vote.json())
