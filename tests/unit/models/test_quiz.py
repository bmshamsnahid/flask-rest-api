from models.quiz import QuizModel
from tests.unit.unit_base_test import UnitBaseTest

class QuizTest(UnitBaseTest):
    def test_create_quiz(self):
        quiz = QuizModel('test_name', 'test_type', 1)
        self.assertEqual(quiz.name, 'test_name')
        self.assertEqual(quiz.type, 'test_type')
        self.assertEqual(quiz.poll_id, 1)

    def test_quiz_json(self):
        quiz = QuizModel('test_name', 'test_type', 1)
        expected = {
            'id': None,
            'name': 'test_name',
            'type': 'test_type',
            'poll_id': 1,
            'options': [],
        }
        self.assertDictEqual(expected, quiz.json())
