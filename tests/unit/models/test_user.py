import json

from tests.unit.unit_base_test import UnitBaseTest
from models.user import UserModel

class UserTest(UnitBaseTest):
    def test_create_user(self):
        user = UserModel('test_username',
                            'test_password',
                            'test_email',
                            'test_role')

        self.assertEqual(user.username, 'test_username')
        self.assertEqual(user.password, 'test_password')
        self.assertEqual(user.email, 'test_email')
        self.assertEqual(user.role, 'test_role')

    def test_user_json(self):
        user = UserModel('test_username',
                            'test_password',
                            'test_email',
                            'test_role')
        expected = {
            'username': 'test_username',
            'email': 'test_email',
            'role': 'test_role',
            'id': None,
        }

        self.assertDictEqual(expected, user.json())
