from models.quiz import QuizModel
from models.poll import PollModel
from tests.base_test import BaseTest

class QuizTest(BaseTest):
    def test_crud(self):
        with self.app_context():
            PollModel('test_title').save_to_db()
            quiz = QuizModel('test_name', 'test_type', 1)

            self.assertIsNone(quiz.find_by_id(1))

            quiz.save_to_db()

            expected_quiz = {
                'id': 1,
                'name': 'test_name',
                'type': 'test_type',
                'poll_id': 1,
                'options': []
            }

            self.assertDictEqual(quiz.find_by_id(1).json(), expected_quiz)
            self.assertDictEqual(quiz.find_by_name('test_name', 1).json(), expected_quiz)
            self.assertListEqual([quiz.json() for quiz in quiz.find_by_type('test_type', 1)], [expected_quiz])
            self.assertListEqual([quiz.json() for quiz in quiz.find_by_poll_id(1)], [expected_quiz])

            quiz.delete_from_db()

            self.assertIsNone(quiz.find_by_id(1))
            self.assertIsNone(quiz.find_by_name('test_name', 1))
            self.assertListEqual(quiz.find_by_type('test_type', 1), [])
            self.assertListEqual(quiz.find_by_poll_id(1), [])
