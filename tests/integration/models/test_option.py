from models.poll import PollModel
from models.quiz import QuizModel
from models.option import OptionModel
from tests.base_test import BaseTest

class OptionTest(BaseTest):
    def test_crud(self):
        with self.app_context():
            PollModel('test_title').save_to_db()
            QuizModel('test_name', 'test_type', 1).save_to_db()
            option = OptionModel('test_name', 1)

            self.assertIsNone(option.find_by_id(1))

            option.save_to_db()

            expected_option = {
                'id': 1,
                'name': 'test_name',
                'quiz_id': 1
            }

            self.assertDictEqual(option.find_by_id(1).json(), expected_option)
            self.assertDictEqual(option.find_by_name('test_name', 1).json(), expected_option)
            self.assertListEqual([option.json() for option in option.find_by_quiz_id(1)], [expected_option])

            option.delete_from_db()

            self.assertIsNone(option.find_by_id(1))

