from models.user import UserModel
from models.poll import PollModel
from models.quiz import QuizModel
from models.option import OptionModel
from models.vote import VoteModel
from tests.base_test import BaseTest

class VoteTest(BaseTest):
    def test_crud(self):
        with self.app_context():
            UserModel('test_username',
                    'test_password',
                    'test_email',
                    'test_role').save_to_db()
            PollModel('test_title').save_to_db()
            QuizModel('test_name', 'test_type', 1).save_to_db()
            OptionModel('test_name', 1).save_to_db()
            vote = VoteModel(1, 1, 1, 1)

            self.assertIsNone(vote.find_by_id(1))

            vote.save_to_db()

            expected_vote = {
                'id': 1,
                'user_id': 1,
                'poll_id': 1,
                'quiz_id': 1,
                'option_id': 1,
            }

            self.assertIsNotNone(vote.find_by_id(1).json(), expected_vote)
            self.assertListEqual([vote.json() for vote in vote.find_by_quiz_id(1)], [expected_vote])
            self.assertListEqual([vote.json() for vote in vote.find_by_option_id(1)], [expected_vote])
            self.assertListEqual([vote.json() for vote in vote.find_by_user_id(1)], [expected_vote])
            self.assertDictEqual(vote.find_by_user_id_and_quiz_id(1, 1).json(), expected_vote)

            vote.delete_from_db()

            self.assertIsNone(vote.find_by_id(1))
