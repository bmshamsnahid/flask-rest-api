from models.user import UserModel
from tests.base_test import BaseTest

class UserTest(BaseTest):
    def test_crud(self):
        with self.app_context():
            user = UserModel('test_username',
                                'test_password',
                                'test_email',
                                'test_role')
            self.assertIsNone(UserModel.find_by_username('test_username'))

            user.save_to_db()

            expected_user = {
                'id': 1,
                'username': 'test_username',
                'email': 'test_email',
                'role': 'test_role',
            }

            self.assertDictEqual(UserModel.find_by_username('test_username').json(), expected_user)
            self.assertIsNotNone(UserModel.find_by_email('test_email').json(), expected_user)
            received_user_list_by_role_filter = [quiz.json() for quiz in UserModel.find_by_role('test_role')]
            self.assertListEqual(received_user_list_by_role_filter,
                                    [expected_user])
            self.assertEqual(UserModel.find_by_id(1).json(), expected_user)

            user.delete_from_db()

            self.assertIsNone(UserModel.find_by_id(1))
            self.assertIsNone(UserModel.find_by_username('test_username'))
            self.assertIsNone(UserModel.find_by_email('test_email'))
