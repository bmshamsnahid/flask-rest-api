from models.user import UserModel
from models.poll import PollModel
from models.quiz import QuizModel
from models.option import OptionModel
from models.vote import VoteModel
from tests.base_test import BaseTest

class PollTest(BaseTest):
    def test_crud(self):
        with self.app_context():
            user = UserModel('test_username', 'test_password', 'test_email', 'test_role')
            poll = PollModel('test_title')
            quiz = QuizModel('test_name', 'test_type', 1)
            option = OptionModel('test_name', 1)
            vote = VoteModel(1, 1, 1, 1)


            self.assertIsNone(PollModel.find_by_title('test_title'))

            poll.save_to_db()

            expected_poll = {
                'id': 1,
                'title': 'test_title',
                'quizes': []
            }

            self.assertDictEqual(PollModel.find_by_id(1).json(), expected_poll)
            self.assertDictEqual(PollModel.find_by_title('test_title').json(), expected_poll)

            poll.delete_from_db()

            self.assertIsNone(PollModel.find_by_id(1))
            self.assertIsNone(PollModel.find_by_title('test_title'))
