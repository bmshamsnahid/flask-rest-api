import json

from models.user import UserModel
from models.poll import PollModel
from models.quiz import QuizModel
from models.option import OptionModel
from tests.base_test import BaseTest

class QuizTest(BaseTest):
    def setUp(self):
        super(QuizTest, self).setUp()
        self.get_multiple_role_access_tokens()

    def test_create_quiz_with_invalid_property(self):
        pass

    def test_create_quiz_with_invalid_authorization(self):
        pass

    def test_create_quiz(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test title').save_to_db()
                response = self.create_post_request(client, {
                                'name': 'test_name',
                                'type': 'drop_down_list',
                                'poll_id': 1
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token,
                            })
                self.assertEqual(response.status_code, 201)

    def test_get_quiz_with_invalid_parameter(self):
        pass

    def test_get_quiz_with_invalid_authorization(self):
        pass

    def test_get_quiz(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test title').save_to_db()
                QuizModel('test name', 'drop_down_list', 1).save_to_db()
                response = self.create_get_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token,
                            })
                expected_output = {
                    'id': 1,
                    'name': 'test name',
                    'type': 'drop_down_list',
                    'poll_id': 1,
                    'options': []
                }

                self.assertEqual(response.status_code, 200)
                self.assertDictEqual(expected_output, json.loads(response.data))

    def test_update_quiz_with_invalid_property(self):
        pass

    def test_update_quiz(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test title').save_to_db()
                QuizModel('test name', 'drop_down_list', 1).save_to_db()

                response = self.create_put_request(client, 1, {
                                'name': 'updated_test_name',
                                'type': 'drop_down_list',
                                'poll_id': 1
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token,
                            })
                expected_output = {
                    'id': 1,
                    'name': 'updated_test_name',
                    'type': 'drop_down_list',
                    'poll_id': 1,
                    'options': []
                }
                self.assertEqual(response.status_code, 201)
                self.assertDictEqual(json.loads(response.data), expected_output)

    def test_delete_quiz_with_invalid_parameter(self):
        pass

    def test_delete_quiz(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test title').save_to_db()
                QuizModel('test name', 'drop_down_list', 1).save_to_db()
                OptionModel('test_name_1', 1).save_to_db()
                OptionModel('test_name_2', 1).save_to_db()

                response = self.create_delete_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token,
                            })
                expected_output = { 'message': 'quiz deleted', 'id': '1' }

                self.assertEqual(response.status_code, 201)
                self.assertDictEqual(expected_output, json.loads(response.data))

                childOptions = OptionModel.query.all()

                self.assertEqual(0, len(childOptions))

    def test_get_quiz_list(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test_title').save_to_db()
                QuizModel('test_name1', 'drop_down_list', 1).save_to_db()
                QuizModel('test_name2', 'drop_down_list', 1).save_to_db()

                response = client.get('/quizes?id=1', headers={
                                                    'Content-Type': 'application/json',
                                                    'Authorization': self.admin_access_token
                                                })
                self.assertEqual(response.status_code, 200)
                self.assertEqual(len(json.loads(response.data)['quizes']), 2)

    def get_access_token(self, username, password, email, role):
        headers = {'Content-Type': 'application/json'}
        with self.app() as client:
            with self.app_context():
                client.post('/register',
                                data=json.dumps({
                                    'username': username,
                                    'password': password,
                                    'email': email,
                                    'role': role,
                                }),
                                headers={'Content-Type': 'application/json'})

                response = client.post('/login',
                                data=json.dumps({
                                    'username': username,
                                    'password': password,
                                }),
                                headers={'Content-Type': 'application/json'})
                access_token = json.loads(response.data)['access_token']
                return access_token

    def get_multiple_role_access_tokens(self):
        self.user_access_token = 'Bearer ' + self.get_access_token('regular_user', 'regular_user_password', 'regular_user_mail', 'user')
        self.admin_access_token = 'Bearer ' + self.get_access_token('admin_user', 'admin_user_password', 'admin_user_mail', 'admin')
        self.super_admin_access_token = 'Bearer ' + self.get_access_token('super_admin_user', 'super_admin_user_password', 'super_admin_user_mail', 'super_admin')

    def create_post_request(self, client, data, headers):
        response = client.post('/quiz',
                                data=json.dumps(data),
                                headers=headers)
        return response

    def create_get_request(self, client, id, headers):
        response = client.get(f'/quiz?id={id}',
                                headers=headers)
        return response

    def create_put_request(self, client, id, data, headers):
        response = client.put(f'/quiz?id={id}',
                                data=json.dumps(data),
                                headers=headers)
        return response

    def create_delete_request(self, client, id, headers):
        response = client.delete(f'/quiz?id={id}',
                                headers=headers)
        return response
