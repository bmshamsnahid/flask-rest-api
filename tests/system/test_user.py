import json

from models.user import UserModel
from tests.base_test import BaseTest

class UserTest(BaseTest):

    def test_register_with_incomplete_data(self):
        with self.app() as client:
            with self.app_context():
                headers = {'Content-Type': 'application/json'}

                response = client.post('/register', data=json.dumps({
                    'password': 'test_password',
                    'email': 'test_email',
                    'role': 'user'
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

                response = client.post('/register', data=json.dumps({
                    'username': 'use',
                    'password': 'test_password',
                    'email': 'test_email',
                    'role': 'user'
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

                response = client.post('/register', data=json.dumps({
                    'username': 'username username',
                    'email': 'test_email',
                    'role': 'user'
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

                response = client.post('/register', data=json.dumps({
                    'username': 'username username',
                    'password': 'test',
                    'email': 'test_email',
                    'role': 'user'
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

                response = client.post('/register', data=json.dumps({
                    'username': 'username username',
                    'password': 'test_password',
                    'role': 'user'
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

                response = client.post('/register', data=json.dumps({
                    'username': 'username username',
                    'password': 'test_password',
                    'email': 'test',
                    'role': 'user'
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

                response = client.post('/register', data=json.dumps({
                    'username': 'username username',
                    'password': 'test_password',
                    'email': 'test_email',
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

                response = client.post('/register', data=json.dumps({
                    'username': 'username username',
                    'password': 'test_password',
                    'email': 'test_email',
                    'role': 'rol'
                }), headers=headers)
                self.assertEqual(response.status_code, 400)

    def test_register_user_with_duplicate_username(self):
        with self.app() as client:
            with self.app_context():
                data1 = json.dumps({
                                    'username': 'test_username',
                                    'password': 'test_password',
                                    'email': 'test_email1',
                                    'role': 'user',
                                })
                data2 = json.dumps({
                                    'username': 'test_username',
                                    'password': 'test_password',
                                    'email': 'test_email2',
                                    'role': 'user',
                                })
                headers = {'Content-Type': 'application/json'}

                client.post('/register', data=data1, headers=headers)
                response = client.post('/register', data=data2, headers=headers)
                expected_message = {
                    'message': 'A user with that username already exists.'
                }
                self.assertEqual(response.status_code, 400)
                self.assertDictEqual(json.loads(response.data), expected_message)

    def test_register_user_with_duplicate_email(self):
        with self.app() as client:
            with self.app_context():
                data1 = json.dumps({
                                    'username': 'test_username1',
                                    'password': 'test_password',
                                    'email': 'test_email',
                                    'role': 'user',
                                })
                data2 = json.dumps({
                                    'username': 'test_username2',
                                    'password': 'test_password',
                                    'email': 'test_email',
                                    'role': 'user',
                                })
                headers = {'Content-Type': 'application/json'}

                client.post('/register', data=data1, headers=headers)
                response = client.post('/register', data=data2, headers=headers)
                expected_message = {
                    'message': 'A user with that email already exists.'
                }
                self.assertEqual(response.status_code, 400)
                self.assertDictEqual(json.loads(response.data), expected_message)

    def test_register_user(self):
        with self.app() as client:
            with self.app_context():
                response = client.post('/register',
                                data=json.dumps({
                                    'username': 'test_username',
                                    'password': 'test_password',
                                    'email': 'test_email',
                                    'role': 'user',
                                }),
                                headers={'Content-Type': 'application/json'})
                expected_message = {
                    'message': 'User created successfully.'
                }
                self.assertEqual(response.status_code, 201)
                self.assertDictEqual(json.loads(response.data), expected_message)

    def test_invalid_login(self):
        with self.app() as client:
            with self.app_context():
                response = client.post('/login',
                                data=json.dumps({
                                    'username': 'test_username',
                                    'password': 'test_password',
                                }),
                                headers={'Content-Type': 'application/json'})

                expected_message = {
                    'message': 'Invalid credentials.'
                }
                self.assertEqual(response.status_code, 401)
                self.assertDictEqual(json.loads(response.data), expected_message)

    def test_login(self):
        with self.app() as client:
            with self.app_context():
                client.post('/register',
                                data=json.dumps({
                                    'username': 'test_username',
                                    'password': 'test_password',
                                    'email': 'test_email',
                                    'role': 'user',
                                }),
                                headers={'Content-Type': 'application/json'})

                response = client.post('/login',
                                data=json.dumps({
                                    'username': 'test_username',
                                    'password': 'test_password',
                                }),
                                headers={'Content-Type': 'application/json'})


                self.assertEqual(response.status_code, 200)
                self.assertIn('access_token', json.loads(response.data).keys())
