import json

from models.user import UserModel
from models.poll import PollModel
from models.quiz import QuizModel
from models.option import OptionModel
from tests.base_test import BaseTest

class OptionTest(BaseTest):
    def setUp(self):
        super(OptionTest, self).setUp()
        self.get_multiple_role_access_tokens()

    def test_create_option_with_invalid_property(self):
        pass

    def test_create_option_with_invalid_authorization(self):
        pass

    def test_create_option(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test_title').save_to_db()
                QuizModel('test_name', 'drop_down_list', 1).save_to_db()
                response = self.create_post_request(client, {
                                'name': 'test_name',
                                'quiz_id': 1
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token,
                            })

                expected_output = {
                    'id': 1,
                    'name': 'test_name',
                    'quiz_id': 1
                }
                self.assertEqual(response.status_code, 201)
                self.assertDictEqual(json.loads(response.data), expected_output)

    def test_get_option_with_invalid_parameter(self):
        pass

    def test_get_option_with_invalid_authorization(self):
        pass

    def test_get_option(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test_title').save_to_db()
                QuizModel('test_name', 'drop_down_list', 1).save_to_db()
                OptionModel('test_name', 1).save_to_db()
                response = self.create_get_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token,
                            })

                expected_output = {
                    'id': 1,
                    'name': 'test_name',
                    'quiz_id': 1
                }

                self.assertEqual(response.status_code, 201)
                self.assertDictEqual(json.loads(response.data), expected_output)

    def test_update_option_with_invalid_property(self):
        pass

    def test_update_option(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test_title').save_to_db()
                QuizModel('test_name', 'drop_down_list', 1).save_to_db()
                OptionModel('test_name', 1).save_to_db()

                response = self.create_put_request(client, 1, {
                                'name': 'updated test_name',
                                'quiz_id': 1
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token,
                            })

                expected_output = {
                    'id': 1,
                    'name': 'updated test_name',
                    'quiz_id': 1
                }
                self.assertEqual(response.status_code, 201)
                self.assertDictEqual(json.loads(response.data), expected_output)

    def test_delete_option_with_invalid_parameter(self):
        pass

    def test_delete_option(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test_title').save_to_db()
                QuizModel('test_name', 'drop_down_list', 1).save_to_db()
                OptionModel('test_name', 1).save_to_db()

                response = self.create_delete_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token,
                            })

                expected_output = {
                    'message': 'Option deleted'
                }

                self.assertEqual(response.status_code, 201)
                self.assertDictEqual(json.loads(response.data), expected_output)

    def test_get_option_list(self):
        with self.app() as client:
            with self.app_context():
                PollModel('test_title').save_to_db()
                QuizModel('test_name', 'drop_down_list', 1).save_to_db()
                OptionModel('test_name', 1).save_to_db()
                response = client.get('/options?id=1',
                                headers={
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token,
                            })

                expected_output = [{
                    'id': 1,
                    'name': 'test_name',
                    'quiz_id': 1
                }]

                self.assertEqual(response.status_code, 200)
                self.assertListEqual(json.loads(response.data)['options'], expected_output)

    def get_access_token(self, username, password, email, role):
        headers = {'Content-Type': 'application/json'}
        with self.app() as client:
            with self.app_context():
                client.post('/register',
                                data=json.dumps({
                                    'username': username,
                                    'password': password,
                                    'email': email,
                                    'role': role,
                                }),
                                headers={'Content-Type': 'application/json'})

                response = client.post('/login',
                                data=json.dumps({
                                    'username': username,
                                    'password': password,
                                }),
                                headers={'Content-Type': 'application/json'})
                access_token = json.loads(response.data)['access_token']
                return access_token

    def get_multiple_role_access_tokens(self):
        self.user_access_token = 'Bearer ' + self.get_access_token('regular_user', 'regular_user_password', 'regular_user_mail', 'user')
        self.admin_access_token = 'Bearer ' + self.get_access_token('admin_user', 'admin_user_password', 'admin_user_mail', 'admin')
        self.super_admin_access_token = 'Bearer ' + self.get_access_token('super_admin_user', 'super_admin_user_password', 'super_admin_user_mail', 'super_admin')

    def create_post_request(self, client, data, headers):
        response = client.post('/option',
                                data=json.dumps(data),
                                headers=headers)
        return response

    def create_get_request(self, client, id, headers):
        response = client.get(f'/option?id={id}',
                                headers=headers)
        return response

    def create_put_request(self, client, id, data, headers):
        response = client.put(f'/option?id={id}',
                                data=json.dumps(data),
                                headers=headers)
        return response

    def create_delete_request(self, client, id, headers):
        response = client.delete(f'/option?id={id}',
                                headers=headers)
        return response
