import json

from models.user import UserModel
from models.poll import PollModel
from models.quiz import QuizModel
from models.option import OptionModel
from models.vote import VoteModel
from tests.base_test import BaseTest

class PollTest(BaseTest):
    def setUp(self):
        super(PollTest, self).setUp()
        self.get_multiple_role_access_tokens()

    def test_create_poll(self):
        with self.app() as client:
            with self.app_context():
                response = self.create_post_request(client, {
                                'title': 'test_title1'
                            }, {
                                'Content-Type': 'application/json',
                            })
                self.assertEqual(response.status_code, 401)

                response = self.create_post_request(client, {
                                'title': 'test_title1'
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token
                            })
                self.assertEqual(response.status_code, 404)

                response = self.create_post_request(client, {
                                'title': 'test_title2'
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token
                            })
                self.assertEqual(response.status_code, 200)

                response = self.create_post_request(client, {
                                'title': 'test_title3'
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.super_admin_access_token
                            })
                self.assertEqual(response.status_code, 200)

    def test_get_poll(self):
        with self.app() as client:
            with self.app_context():
                response = self.create_get_request(client, 1, {
                                'Content-Type': 'application/json',
                            })
                self.assertEqual(response.status_code, 401)

                response = self.create_get_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token
                            })
                self.assertEqual(response.status_code, 404)

                PollModel('test title').save_to_db()
                response = self.create_get_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token
                            })
                self.assertEqual(response.status_code, 200)

    def test_update_poll(self):
        with self.app() as client:
            with self.app_context():
                response = self.create_put_request(client, 1, {
                                'title': 'test_title'
                            }, {
                                'Content-Type': 'application/json',
                            })
                self.assertEqual(response.status_code, 401)

                PollModel('test_title').save_to_db()

                response = self.create_put_request(client, 1, {
                                'title': 'test_title'
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token
                            })
                self.assertEqual(response.status_code, 404)

                response = self.create_put_request(client, 1, {
                                'title': 'test_title'
                            }, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token
                            })
                self.assertEqual(response.status_code, 200)

    def test_delete_poll(self):
        with self.app() as client:
            with self.app_context():
                # failed with empty token
                response = self.create_delete_request(client, 1, {
                                'Content-Type': 'application/json',
                            })
                self.assertEqual(response.status_code, 401)

                # failed with user access
                response = self.create_delete_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.user_access_token
                            })
                self.assertEqual(response.status_code, 404)

                # failed with poll does not exists
                response = self.create_delete_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token
                            })
                self.assertEqual(response.status_code, 404)

                PollModel('test_title').save_to_db()
                UserModel('test_username', 'test_password', 'test_email', 'user').save_to_db()
                QuizModel('test_name 01', 'drop_down_list', 1).save_to_db()
                QuizModel('test_name 02', 'drop_down_list', 1).save_to_db()
                OptionModel('test_name', 1).save_to_db()
                OptionModel('test_name', 2).save_to_db()
                OptionModel('test_name', 2).save_to_db()
                VoteModel(1, 1, 1, 1).save_to_db()
                VoteModel(1, 1, 2, 1).save_to_db()

                votes = VoteModel.query.all()

                # success, poll deleted
                response = self.create_delete_request(client, 1, {
                                'Content-Type': 'application/json',
                                'Authorization': self.admin_access_token
                            })

                expected_output = { 'message': 'Poll deleted.', 'id': '1' }

                self.assertEqual(response.status_code, 200)
                self.assertDictEqual(expected_output, json.loads(response.data))

                quizes = QuizModel.query.all()
                options = OptionModel.query.all()
                votes = VoteModel.query.all()

                # since parent poll is deleted, child option should be null
                self.assertEqual(0, len(quizes))
                self.assertEqual(0, len(options))
                self.assertEqual(0, len(votes))

    def test_get_poll_list(self):
        with self.app() as client:
            with self.app_context():
                response = client.get('/polls', headers={'Content-Type': 'application/json'})
                self.assertEqual(response.status_code, 401)
                response = client.get('/polls', headers={
                                                    'Content-Type': 'application/json',
                                                    'Authorization': self.user_access_token
                                                })
                self.assertEqual(response.status_code, 200)
                self.assertListEqual(json.loads(response.data)['polls'], [])

                PollModel('test title1').save_to_db()
                PollModel('test title2').save_to_db()
                PollModel('test title3').save_to_db()

                QuizModel('test quiz 1', 'test_type', 1).save_to_db()
                QuizModel('test quiz 2', 'test_type', 1).save_to_db()

                QuizModel('test quiz 1', 'test_type', 3).save_to_db()

                OptionModel('name 01', 1).save_to_db()
                OptionModel('name 02', 1).save_to_db()

                OptionModel('name 01', 3).save_to_db()

                expected_output = [
                    {
                        'id': 1,
                        'title': 'test title1',
                        'quizes': [
                            {
                                'id': 1,
                                'name': 'test quiz 1',
                                'options': [
                                    {
                                        'id': 1,
                                        'name': 'name 01',
                                        'quiz_id': '1',
                                    },
                                    {
                                        'id': 2,
                                        'name': 'name 02',
                                        'quiz_id': '1',
                                    },
                                ],
                                'poll_id': 1,
                                'type': 'test_type'
                            },
                            {
                                'id': 2,
                                'name': 'test quiz 2',
                                'options': [],
                                'poll_id': 1,
                                'type': 'test_type'
                            }
                        ],
                    },
                    {
                        'id': 2,
                        'title': 'test title2',
                        'quizes': [],
                    },
                    {
                        'id': 3,
                        'title': 'test title3',
                        'quizes': [
                            {
                                'id': 1,
                                'name': 'test quiz 1',
                                'options': [
                                    {
                                        'id': 3,
                                        'name': 'name 02',
                                        'quiz_id': '1',
                                    },
                                ],
                                'poll_id': 1,
                                'type': 'test_type'
                            },
                        ],
                    },
                ]

                response = client.get('/polls', headers={
                                                    'Content-Type': 'application/json',
                                                    'Authorization': self.admin_access_token
                                                })

                received_polls = json.loads(response.data)

                number_of_polls = len(expected_output)

                number_of_quizes_for_first_poll = len(expected_output[0]['quizes'])
                number_of_quizes_for_third_poll = len(expected_output[2]['quizes'])

                number_of_options_for_first_poll_first_quiz = len(expected_output[0]['quizes'][0]['options'])
                number_of_options_for_third_poll_first_quiz = len(expected_output[2]['quizes'][0]['options'])

                self.assertEqual(len(received_polls['polls']), number_of_polls)

                self.assertEqual(len(received_polls['polls'][0]['quizes']), number_of_quizes_for_first_poll)
                self.assertEqual(len(received_polls['polls'][2]['quizes']), number_of_quizes_for_third_poll)

                self.assertEqual(len(received_polls['polls'][0]['quizes'][0]['options']), number_of_options_for_first_poll_first_quiz)
                self.assertEqual(len(received_polls['polls'][2]['quizes'][0]['options']), number_of_options_for_third_poll_first_quiz)

    def get_access_token(self, username, password, email, role):
        headers = {'Content-Type': 'application/json'}
        with self.app() as client:
            with self.app_context():
                client.post('/register',
                                data=json.dumps({
                                    'username': username,
                                    'password': password,
                                    'email': email,
                                    'role': role,
                                }),
                                headers={'Content-Type': 'application/json'})

                response = client.post('/login',
                                data=json.dumps({
                                    'username': username,
                                    'password': password,
                                }),
                                headers={'Content-Type': 'application/json'})
                access_token = json.loads(response.data)['access_token']
                return access_token

    def get_multiple_role_access_tokens(self):
        self.user_access_token = 'Bearer ' + self.get_access_token('regular_user', 'regular_user_password', 'regular_user_mail', 'user')
        self.admin_access_token = 'Bearer ' + self.get_access_token('admin_user', 'admin_user_password', 'admin_user_mail', 'admin')
        self.super_admin_access_token = 'Bearer ' + self.get_access_token('super_admin_user', 'super_admin_user_password', 'super_admin_user_mail', 'super_admin')

    def create_post_request(self, client, data, headers):
        response = client.post('/poll',
                                data=json.dumps(data),
                                headers=headers)
        return response

    def create_get_request(self, client, id, headers):
        response = client.get(f'/poll?id={id}',
                                headers=headers)
        return response

    def create_put_request(self, client, id, data, headers):
        response = client.put(f'/poll?id={id}',
                                data=json.dumps(data),
                                headers=headers)
        return response

    def create_delete_request(self, client, id, headers):
        response = client.delete(f'/poll?id={id}',
                                headers=headers)
        return response
