# Poll System

## Prerequisite Technologies In Your Machine
* [Python](https://www.python.org/)


## Installation

```
git clone 'repo_address'
cd 'root_directory'
source venv/Scripts/activate
python app.py
```

```
DATABASE FOR PRODUCTION: `PollSystemTest`
DATABASE FOR TEST: `test`
```

NOW API IS AVAILABLE (http://localhost:5000)

## Test

```
python test_suite.py
```
