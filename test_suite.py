import unittest

from tests.unit.models.test_poll import PollTest as UnitPollTest
from tests.unit.models.test_quiz import QuizTest as UnitQuizTest
from tests.unit.models.test_user import UserTest as UnitUserTest
from tests.unit.models.test_option import OptionTest as UnitOptionTest
from tests.unit.models.test_vote import VoteTest as UnitVoteTest

from tests.integration.models.test_user import UserTest as IntegrationUserTest
from tests.integration.models.test_poll import PollTest as IntegrationPollTest
from tests.integration.models.test_quiz import QuizTest as IntegrationQuizTest
from tests.integration.models.test_option import OptionTest as IntegrationOptionTest
from tests.integration.models.test_vote import VoteTest as IntegrationVoteTest

from tests.system.test_user import UserTest as SystemUserTest
from tests.system.test_poll import PollTest as SystemPollTest
from tests.system.test_quiz import QuizTest as SystemQuizTest
from tests.system.test_option import OptionTest as SystemOptionTest
from tests.system.test_vote import VoteTest as SystemVoteTest

def test_unit_model():
    suite = unittest.TestSuite()
    result = unittest.TestResult()

    suite.addTest(unittest.makeSuite(UnitPollTest))
    suite.addTest(unittest.makeSuite(UnitQuizTest))
    suite.addTest(unittest.makeSuite(UnitUserTest))
    suite.addTest(unittest.makeSuite(UnitOptionTest))
    suite.addTest(unittest.makeSuite(UnitVoteTest))

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)

def test_integration_model():
    suite = unittest.TestSuite()
    result = unittest.TestResult()

    suite.addTest(unittest.makeSuite(IntegrationUserTest))
    suite.addTest(unittest.makeSuite(IntegrationPollTest))
    suite.addTest(unittest.makeSuite(IntegrationQuizTest))
    suite.addTest(unittest.makeSuite(IntegrationOptionTest))
    suite.addTest(unittest.makeSuite(IntegrationVoteTest))

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)

def test_system():
    suite = unittest.TestSuite()
    result = unittest.TestResult()

    suite.addTest(unittest.makeSuite(SystemUserTest))
    suite.addTest(unittest.makeSuite(SystemPollTest))
    suite.addTest(unittest.makeSuite(SystemQuizTest))
    suite.addTest(unittest.makeSuite(SystemOptionTest))
    suite.addTest(unittest.makeSuite(SystemVoteTest))

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)

test_unit_model()
test_integration_model()
test_system()
